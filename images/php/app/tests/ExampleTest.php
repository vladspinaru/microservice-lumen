<?php

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->get('/');

        $this->assertEquals(
            $this->app->version(), $this->response->getContent()
        );
    }

    /**
     * Test JSON.
     *
     * @return void
     */
    public function testJSONisReturnedOnList()
    {
        $this->get('/list');

        $this->assertJsonStringEqualsJsonString(json_encode(['10' => 'Title 10', '11' => 'Title 11']), $this->response->getContent());
    }
}
