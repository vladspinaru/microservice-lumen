<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Illuminate\Support\Facades\Crypt;

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/list', function () use ($router) {
    return json_encode(['10' => 'Title 10', '11' => 'Title 11']);
});

$router->get('/cache', function () use ($router) {
    if (!Cache::has('current_time')) {
        Cache::put('current_time', date('Y-m-d H:i:s'), 10);
    }

    return Cache::get('current_time');
});

$router->get('/crypt', function () use ($router) {
    $encrypted = Crypt::encrypt('secret');

    try {
        $decrypted = Crypt::decrypt($encrypted);
    } catch (DecryptException $e) {
        echo 'Error in decryption !';
    }

    return json_encode([$encrypted, $decrypted]);
});
